var O_E = {
	init : function(){
		chrome.runtime && $('#optionsLayout .option-header-inner #extVersion').html(chrome.runtime.getManifest().version);

		this.tempIndex = 0;
		this.tempArr = [];
		this.calculator_noneLi();
		this.eventHandle();
	}

	,calculator_noneLi : function(){
		let liHeight = 0;
		$('ul.list-option li:not(.li-none)').each((i,v)=>{
			liHeight += $(v).outerHeight();
		})
		$('li.li-none').css('min-height', 'calc(100% - '+liHeight+'px)');
	}

	,eventHandle : ()=>{

		chrome.storage && chrome.storage.sync.get(['ArenaData_checkbox'],(result)=>{
			let _checkboxArray 	= result['ArenaData_checkbox']

			if (_checkboxArray !== undefined) {

				for (var i = 0; i < _checkboxArray.length; i++) {

					let _element = '#'+_checkboxArray[i].id
					,_checked = _checkboxArray[i].value;

					$(_element).prop('checked',_checked);
				}
			}

			O_E.setData();
		})

		$(document)
		.on('click','#saveOptions', (e)=>{
			O_E.setData();
			chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
				chrome.tabs.sendMessage(tabs[0].id, "arnExt_changed");
			});

			$(e.currentTarget).html('Saving...').delay(700).promise().done(()=>$(e.currentTarget).delay(1500).html('Save'));

		})
		.on('click','ul.list-option li:not(.li-none)', (e)=>{
			$('ul.list-option li').removeClass('active');
			$(e.currentTarget).addClass('active');
			$('.option-tab').removeClass('active').removeAttr('style');
			$('#'+$(e.currentTarget).attr('data-target')).fadeIn().addClass('active');
			O_E.calculator_noneLi();
		})
	}

	,setData : ()=>{
		let _array = [];

		$('input.areExtension-checkbox').each((i,v)=>{
			let _value 	= $(v).prop('checked'),
			_key 	= $(v).attr('id');

			_array.push({id:_key,value:_value});
		});

		chrome.storage && chrome.storage.sync.set({'ArenaData_checkbox': _array});
	}
}

O_E.init();