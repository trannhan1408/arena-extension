(function(){
	let scriptID = document.getElementById(`arn_extscript`);
	let key = scriptID.src.split('?v=')[1];
	let pathURL = location.pathname;
	let shopDomain = '' ;
	try {
		shopDomain = location.protocol + '//' + Shopify.shop;
	} catch(e) {
		shopDomain = location.protocol + '//' + location.host;
		console.log(e);
	}


	if(key == 'sections_editor'){

		let url = [shopDomain, '/admin/themes'].join('');
		let searchURL = `https://partners.shopify.com/565271/stores?search_value=${shopDomain}&sort_order=created_at_desc`
		let t = document.querySelector('#arnExtension_quicklink .arnExt-body-inner');
		let target = document.getElementById('arnExt_storeURL');
		target != null && target.remove();

		t.insertAdjacentHTML('beforeend', `<div id="arnExt_storeURL">
			<style>#arnExt_storeURL{text-align: center;padding-bottom: 15px;}
			#arnExt_storeURL a{color: #7084cb;font-weight: bold;text-decoration: underline;}
			#arnExt_storeURL span{display: block;}</style>
			<a href="${searchURL}" target="_blank" style="margin-right: 10px;">Search store</a>
			<a href="${url}" target="_blank">Go to store</a>
			<span>${shopDomain}</span>
			</div>`);

		return;
	}

	let openURL = '';

	if(key == 'editor_customize'){
		openURL = `${shopDomain}/admin/themes/${Shopify.theme.id}/editor`;
		(pathURL !== '/') && (openURL+=(`?previewPath=${encodeURI(pathURL)}`));
	}

	if(key == 'edit_content'){

		if (pathURL === '/' || pathURL === '/collections/all') {
			alert('Not support for this page :)');
		}
		else{
			openURL = `${shopDomain}/admin/${__st.p}`;
			(pathURL !== '/collections') && (openURL+=`s/${__st.rid}`)
		}
	}


	if(key == 'customfields'){
		let subPath = `${__st.p}s_editor/${__st.rid}`;
		if(__st.p == 'article'){
			subPath = `posts_editor?id=${__st.rid}&shop=${location.host}`
		}

		openURL = `${shopDomain}/admin/apps/arena-custom-fields/${subPath}`;
	}

	key == 'code_editor' && (openURL = `${shopDomain}/admin/themes/${Shopify.theme.id}`);

	key == 'extra_products' && (openURL = `${shopDomain}/admin/products?limit=250&selectedView=all`);


	!openURL.length && (openURL = `${shopDomain}/admin/${encodeURI(key)}`);

	console.log('key: ', key);
	openURL.length && window.open(openURL, '_blank');

})()