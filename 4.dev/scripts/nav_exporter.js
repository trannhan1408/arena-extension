"use strict";
console.log('nav_exporter');
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  var extensionID = chrome.runtime.id;

  var from = request.from,
      action = request.action;
  var id = sender.id;
  
  const MAX_RETRY_WRITE_SESSION_STORAGE = 2;
  const TIMEOUT_RETRY_WRITE_SESSION_STORAGE = 700; //ms
  // import-menu
  if (from === 'install-app' && action === 'import-menu' && id == extensionID) {
    let client_url = request.client_url,
    menu_string = request.menu_string,
    mapBlogs = request.mapBlogs,
    mapArticles = request.mapArticles,
    mapCollections = request.mapCollections,
    mapProducts = request.mapProducts,
    mapPages = request.mapPages;
    // Embed script  
    if (document.getElementById("arena-import-script")) {
      document.getElementById("arena-import-script").remove();
    }
    if (document.getElementById("arena-import-info")) {
      document.getElementById("arena-import-info").remove();
    }
    var dataScript = document.createElement("script");
    dataScript.type = "text/javascript";
    dataScript.id = "arena-import-info";
    var menu_string_handled = menu_string.replace(/\'/gm, "\\'");
    var code = "\n        var theme_import_info = {\n          menu_string: '".concat(menu_string_handled, "',\n          mapBlogs: '").concat(JSON.stringify(mapBlogs), "',\n          mapCollections: '").concat(JSON.stringify(mapCollections), "',\n          mapProducts: '").concat(JSON.stringify(mapProducts), "',\n          mapPages: '").concat(JSON.stringify(mapPages), "',\n          mapArticles: '").concat(JSON.stringify(mapArticles), "',\n        }\n      ");

    for (let i = 0; i < MAX_RETRY_WRITE_SESSION_STORAGE; i++)
    {
      setTimeout(function() {
        sessionStorage.setItem('menu_string_handled_str', JSON.stringify(menu_string));
        sessionStorage.setItem('mapBlogs_str', JSON.stringify(mapBlogs));
        sessionStorage.setItem('mapCollections_str', JSON.stringify(mapCollections));
        sessionStorage.setItem('mapProducts_str', JSON.stringify(mapProducts));
        sessionStorage.setItem('mapPages_str', JSON.stringify(mapPages));
        sessionStorage.setItem('mapArticles_str', JSON.stringify(mapArticles));
        if (sessionStorage.getItem('ArenaAlreadyRunImport') == null)
        { sessionStorage.setItem('ArenaAlreadyRunImport', 'false'); }
        if (location.href.includes('firstShow=0'))
        { sessionStorage.setItem('urlOpenByChromeExtension', location.href); }
        //sessionStorage.setItem('importRequest', JSON.stringify(request));
        sessionStorage.setItem('ArenaActionFromApp', 'import-menu' );
      }, TIMEOUT_RETRY_WRITE_SESSION_STORAGE);
    }

    // Thanh comment 21/06/2022 - 2 below rows
    // dataScript.appendChild(document.createTextNode(code));
    // document.getElementsByTagName("body")[0].appendChild(dataScript);

    sendResponse({
      status: 'success',
      // msg: 'We attached the script arena-import-info'
      msg: 'We no longer attached the script arena-import-info'
    });
  } 
  // export-menu
  else if (from === 'xadmin' && action === 'export-menu' && id == extensionID) {
    let demo_url = request.demo_url,
    theme_id = request.theme_id,
    theme_version = request.theme_version;
    // Embed script
    if (document.getElementById("arena-export-script")) {
      document.getElementById("arena-export-script").remove();
    }
    if (document.getElementById("arena-export-info")) {
      document.getElementById("arena-export-info").remove();
    }  
    var dataScript = document.createElement("script");
    dataScript.type = "text/javascript";
    dataScript.id = "arena-export-info";
    var code = "\n        var theme_export_info = {\n          demo_url: \"".concat(demo_url, "\",\n          theme_id: \"").concat(theme_id, "\",\n          theme_version: \"").concat(theme_version, "\",\n        }\n      ");
    var theme_export_info = { demo_url : demo_url, theme_id: theme_id, theme_version: theme_version };
  
    // Thanh comment 21/06/2022 - 2 below rows
    // dataScript.appendChild(document.createTextNode(code));
    // document.getElementsByTagName("body")[0].appendChild(dataScript);

    // var exportScript = document.createElement("script");
    // exportScript.type = "text/javascript";
    // exportScript.src = chrome.runtime.getURL('export_script.js');
    // exportScript.id = "arena-export-script";
    // document.getElementsByTagName("body")[0].appendChild(exportScript);

    for (let i = 0; i < MAX_RETRY_WRITE_SESSION_STORAGE; i++)
    {
      setTimeout(function() {
        //sessionStorage.setItem("showExportMenu", 'true');
        sessionStorage.setItem('theme_export_info_str', JSON.stringify(theme_export_info));
        sessionStorage.setItem('ArenaAlreadyRunExport', 'false');
        if (location.href.includes('firstShow=0'))
        {
          sessionStorage.setItem('urlOpenByChromeExtension', location.href);
          sessionStorage.setItem('ArenaUseRestfulForExport', 'true');
        }
        //sessionStorage.setItem('exportRequest', JSON.stringify(request));
        sessionStorage.setItem('ArenaActionFromApp', 'export-menu' );
      }, TIMEOUT_RETRY_WRITE_SESSION_STORAGE);
    }  
    sendResponse({
      status: 'success',
      // msg: 'We attached the script arena-export-script'
      msg: 'We no longer attached the script arena-export-script'
    });
  }  // export-menu
  else {
    sendResponse({
      status: 'error',
      msg: 'ACF Import Export: Invalid message at Nav Exporter'
    });
  }
  // end 
}); // addListener
