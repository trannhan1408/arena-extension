chrome.storage.sync.get(['arnPartner', 'arnToken'], (result)=>{
    document.querySelectorAll('input').forEach(i=>{

        i.type == 'radio' && result['arnPartner'] == i.value && (i.checked = true);
        i.type == 'text' && (i.value = result['arnToken']);

        i.addEventListener('change', e=>{
            if(!chrome.storage) return;

            i.type == 'radio' && chrome.storage.sync.set({'arnPartner': i.value});
            i.type == 'text' && chrome.storage.sync.set({'arnToken': i.value});
        })
    });
})