class AXT {
	constructor() {
		
		this.isShopifyStore() && chrome.storage.sync.get(['arnPartner'], (result)=>{
			this.partnerID = result['arnPartner'];
			this.preload();
		})

		this.CRM();
		// this.loadSupport();
	}

	isShopifyStore(){
		const isPasswordPage = location.pathname == '/password' && document.documentElement.innerHTML.includes('shopify');
		const isShopifyPage = document.querySelectorAll('[href*="shopify"], [src*="shopify"]').length && document.querySelector(`script[id="__st"]`);

		if( isShopifyPage || isPasswordPage) return true;

		return false;
	}

	loadSupport(){
		if(location.host != 'support.arenacommerce.com') return;

		const io = new MutationObserver((entries) => {
			const classList = entries.map(i=>i.target);

			if(classList.some(i=>i.querySelector('.page-actions__left'))){
				this.loadButtonSupport();
				io.disconnect();

				return;
			}
		})

		io.observe(document.body, {childList: true});
	}

	loadButtonSupport(){

		const id = document.querySelector('[name="customFields.cf_shop_owner"]')?.value || '';

		if(!id.trim().length || document.querySelector('.js-crm-toggle')) return;

		document.querySelector('.page-actions__left').insertAdjacentHTML('beforeend', `
			<button data-crm-action="start" aria-label="Start task" class="nucleus-button nucleus-button--secondary app-icon-btn--text hint--rounded hint--bottom ember-view js-crm-toggle" type="button" style="background: #0c3e34; color: #ffffff;">START TASK</button>
			<button data-crm-action="stop" aria-label="Stop task" class="nucleus-button nucleus-button--secondary app-icon-btn--text hint--rounded hint--bottom ember-view js-crm-toggle" type="button" style="background: #c82124; color: #ffffff;">STOP TASK</button>
		`);


		document.querySelectorAll('.js-crm-toggle').forEach(button=>{

			button.style.minWidth = button.clientWidth + 'px';

			button.addEventListener('click', e=>{
				e.preventDefault();
				e.stopPropagation();

				const text = button.textContent;
				button.textContent = '...';
				const msg = button.getAttribute('data-crm-action');
				button.toggleAttribute('disabled', true);

				chrome.runtime.sendMessage({ type: msg, taskId: id }, (response)=>{
					response.msg && setTimeout(()=>{
						button.textContent = text;
						button.toggleAttribute('disabled', false);
					}, 700);
				});
			})
		})
	}

	preload(){
		[
			'resources/arn-extension.min.css'
        	,'resources/arn-extension.js'

		].forEach(async file=>{
			let node;

			if(file.includes('.css')){
				node = document.createElement('link');
				node.href = chrome.runtime.getURL(file);
				node.rel = 'stylesheet';
				node.type = "text/css";
				node.media = "all";
				document.head.insertAdjacentElement('beforeend', node);
			}
			else{

				await new Promise((resolved, rejcet) => {
					node = document.createElement('script');
					node.src = chrome.runtime.getURL(file);
					node.onload = () => {resolved(1);};

					file.includes('arn-main') && node.setAttribute('data-partner-id', this.partnerID);
					file.includes('arn-main') && node.setAttribute('data-token', this.partnerToken);
					document.body.insertAdjacentElement('beforeend', node);
				})
			}
		});
	}

	CRM(){
		
		if(!location.href.includes('admin/si_task_filters/tasks_report?filter_id')) return;

		const node = document.createElement('link');
		node.href = chrome.runtime.getURL('resources/crm.css');
		node.rel = 'stylesheet';
		node.type = "text/css";
		node.media = "all";
		document.body.insertAdjacentElement('beforeend', node);

		const target = document.getElementById('DataTables_Table_0');
		
		target.querySelectorAll('tr > th')?.forEach((i, index)=>{

			i.setAttribute('data-index', index+1);
		})
	}
}

window.AXT = new AXT();