(()=>{
	var importBtn = document.querySelector(".arn-nav-import-btn");

	if (!importBtn) {
		importBtn = document.createElement('button');
		importBtn.className = 'arn-nav-import-btn ui-button ui-button--default ui-title-bar__action';
		importBtn.textContent = location.href.includes('firstShow=1') ? 'Back to Arena Dashboard' : 'Arena Menu Import';
		
		document.querySelector('.ui-title-bar__actions').insertAdjacentElement('beforeend', importBtn);
	}

	importBtn.addEventListener("click",e=>{
		let tabID = location.href.split('tabID=')[1] || '';
		try {
			tabID != '' && chrome.runtime.sendMessage('gohkmbgkecjbdaifhnjejocobijidfei', {method: "switchTab", value: tabID});
		}
		catch(e) {

		}
	})

})()