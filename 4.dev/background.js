/**
 * CONTENTTEXTMENU
 */
var partnerId = 565271;
var partnerToken = null;

chrome.storage.sync.get(['arnPartner', 'arnToken'], (result)=>{
  partnerId = result['arnPartner'] || 565271;
  partnerToken = result['arnToken'] || null;
})

chrome.contextMenus.removeAll(function() {

  chrome.contextMenus.create({
    id:'ska8v0aakbmms',
    title: "Search in Partner stores",
    contexts:["selection","link"]
  });
});

chrome.contextMenus.onClicked.addListener(function(info, tab) {
  if (info.menuItemId == "ska8v0aakbmms") {
    let _selectionText = info.selectionText || info.linkUrl || null;
    if (_selectionText == null) return;
    if (_selectionText.includes('.myshopify') && _selectionText.includes('https://twitter.com/') || _selectionText.includes('.myshopify') && _selectionText.includes('tel:')) {
      _selectionText = _selectionText.replace('https://twitter.com/', '')
      .replace('tel:', '')
      .replace('http://', '')
      .replace('https://', '')
    }
    let _url = `https://partners.shopify.com/${partnerId}/stores?search_value=${_selectionText}&sort_order=created_at_desc&store_type=managed&archived=false`;
    chrome.tabs.create({url: _url});
  }

  if (info.menuItemId == "ska8v0a252mms") {
    let _selectionText = info.selectionText || info.linkUrl;
    if (_selectionText != 'undefined') {
      if (_selectionText.includes('.myshopify') && _selectionText.includes('https://twitter.com/')
        || _selectionText.includes('.myshopify') && _selectionText.includes('tel:')) {
        _selectionText = _selectionText.replace('https://twitter.com/', '').replace('tel:', '');
      }
      let _url = `https://${_selectionText}`;
      chrome.tabs.create({url: _url});
    }
  }
  if (info.menuItemId == "ska8v0a252crmStart" && partnerToken) {
    let id = info.selectionText;


  }
  if (info.menuItemId == "ska8v0a252crmStop" && partnerToken) {
    let id = info.selectionText;

    fetch(`https://crm.arenacommerce.com/arena/api/stop_timer/${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'authtoken': partnerToken
      }
    })
  }
});

const toggleCRM = (type = 'start', id)=>{
  return fetch(`https://crm.arenacommerce.com/arena/api/${type}_timer/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'authtoken': partnerToken
    }
  })
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse)=>{
  if(!partnerToken) return false;

  sendResponse({msg: true});
  toggleCRM(request.type, request.taskId);
});
