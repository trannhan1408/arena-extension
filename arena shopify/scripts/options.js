$(function(){
	chrome.storage.sync.get(['arnData','arnWhitelist'],(result)=>{

		let _rs = result['arnData']
		, _adminBar = _adminBar_H = _fontPicker = _datePicker = _headerCollapsed = _arnSpacing = _arnResp = true;
		
		if (_rs) {
			 _adminBar 			= _rs['adminBar'].value;
			 _adminBar_H 		= _rs['arnAdminbar_hor'].value;
			_fontPicker 		= _rs['fontPicker'].value;
			_datePicker 		= _rs['datePicker'].value;
			_headerCollapsed 	= _rs['headerCollapsed'].value;
			_arnSpacing 		= _rs['arnSpacing'].value
			_arnResp 			= _rs['arnResp'].value;
		}
		$('#adminBar').prop('checked',_adminBar);
		$('#adminBar-horizontal').prop('checked',_adminBar_H);
		$('#fontPicker').prop('checked',_fontPicker);
		$('#datePicker').prop('checked',_datePicker);
		$('#headerCollapsed').prop('checked',_headerCollapsed);
		$('#arnSpacing').prop('checked',_arnSpacing);
		$('#arnResp').prop('checked',_arnResp);
		$('#arn_whitelist').val(result['arnWhitelist']);
		setData();
	});

	$('#adminBar, #fontPicker, #datePicker, #headerCollapsed, #arnSpacing, #arnResp, #adminBar-horizontal').click(function(){
		setData();
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, "arnData_changed");
		});
	})
	$('.op-whitelist .btn-whitelist').click(function(e){
		let _value = $('#arn_whitelist').val().toString();
		chrome.storage.sync.set({'arnWhitelist': _value});
	})
})
var setData = function(){
	let arrData ={  "adminBar":{"value":$('#adminBar').prop("checked")},
					"arnAdminbar_hor":{"value":$('#adminBar-horizontal').prop("checked")},
					"fontPicker":{"value":$('#fontPicker').prop("checked")},
					"datePicker":{"value":$('#datePicker').prop("checked")},
					"headerCollapsed":{"value":$('#headerCollapsed').prop("checked")},
					"arnSpacing":{"value":$('#arnSpacing').prop("checked")},
					"arnResp":{"value":$('#arnResp').prop("checked")}
					}
	chrome.storage.sync.set({'arnData': arrData});
}