$(function(){
	var 	 adminBar 	= '#adminBar'
			,adminHor 	= '#adminBar-horizontal'
			,fontPick	= '#fontPicker'
			,datePick	= '#datePicker'
			,headerCo	= '#headerCollapsed'
			,spacingP 	= '#arnSpacing'
			,responsP 	= '#arnResp'
			,richText 	= '#arnRichText'
			,whiteLis 	= '#arn_whitelist';

	chrome.storage.sync.get(['arnData','arnWhitelist'],(result)=>{

		let _rs = result['arnData'],adminBar_val,adminHor_val,fontPick_val,datePick_val,headerCo_val,spacingP_val,columnRe_val,richText_val;

		if (_rs) {
			adminBar_val = _rs['adminBar'] 		=== undefined ? true : _rs['adminBar'].value;
			adminHor_val = _rs['adminHor'] 		=== undefined ? true : _rs['adminHor'].value;
			fontPick_val = _rs['fontPicker'] 	=== undefined ? true : _rs['fontPicker'].value;
			datePick_val = _rs['datePicker'] 	=== undefined ? true : _rs['datePicker'].value;
			headerCo_val = _rs['headerColl'] 	=== undefined ? true : _rs['headerColl'].value;
			spacingP_val = _rs['spacingResp'] 	=== undefined ? true : _rs['spacingResp'].value
			columnRe_val = _rs['columnResp'] 	=== undefined ? true : _rs['columnResp'].value;
			richText_val = _rs['richTextEdit'] 	=== undefined ? true : _rs['richTextEdit'].value;
		}
		$(adminBar).prop('checked',adminBar_val);
		$(adminHor).prop('checked',adminHor_val);
		$(fontPick).prop('checked',fontPick_val);
		$(datePick).prop('checked',datePick_val);
		$(headerCo).prop('checked',headerCo_val);
		$(spacingP).prop('checked',spacingP_val);
		$(responsP).prop('checked',columnRe_val);
		$(richText).prop('checked',richText_val);
		$(whiteLis).val(result['arnWhitelist']);
		setData();
	});

	$(`${adminBar},${adminHor},${fontPick},${datePick},${headerCo},${spacingP},${responsP},${richText}`).click(function(){
		setData();
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs){chrome.tabs.sendMessage(tabs[0].id, "arnExt_changed");});
	});

	$('.op-whitelist .btn-whitelist').click(function(e){
		let _value = $('#arn_whitelist').val().toString();
		$('.wishlist-label').hide().html('Saving...').fadeIn('700').promise().done(()=>$('.wishlist-label').delay(1500).html('Saved!').fadeOut('300'));
		chrome.storage.sync.set({'arnWhitelist': _value});
	});
	var setData = function(){
		let arrData ={   "adminBar" 	:{"value":$(adminBar).prop("checked")}
						,"adminHor"		:{"value":$(adminHor).prop("checked")}
						,"fontPicker"	:{"value":$(fontPick).prop("checked")}
						,"datePicker"	:{"value":$(datePick).prop("checked")}
						,"headerColl"	:{"value":$(headerCo).prop("checked")}
						,"spacingResp"	:{"value":$(spacingP).prop("checked")}
						,"columnResp"	:{"value":$(responsP).prop("checked")}
						,"richTextEdit"	:{"value":$(richText).prop("checked")}
						}
		chrome.storage.sync.set({'arnData': arrData});
	};
});